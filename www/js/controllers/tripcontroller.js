angular.module('starter.trip', ['shared.createitinerary', 'shared.hotel','service.trip'])
.controller('TripCtrl', function($scope, ModalCreateItinerary, $state, $ionicPlatform, TripService, $model) {

	$scope.trips = []; //init:
	$scope.tripLoader = true; //set true for first time
	$scope.tripEmpty = false;

	$ionicPlatform.ready(function(){ 
		//get my trip onload:
		TripService.loadTrip().then(function(result){
			if(result != null){
				for(var i = 0; i < result.length; i++){
	              	var trip = new $model.Trip(result[i]);
	    			$scope.trips.push(trip);
	            }
			}

			if($scope.trips < 1){
				$scope.tripEmpty = true;
			}

			$scope.tripLoader = false; //once loaded, set it false
		});

	});

	$scope.manageTrip = function(tripid, tripname){
		$state.go('tab.trip-detail', {"tripID":tripid, "tripName":tripname});
	}

	$scope.doReload = function(){
		$scope.trips = []; //reset
		TripService.loadTrip().then(function(result){
			if(result != null){
				for(var i = 0; i < result.length; i++){
	              	var trip = new $model.Trip(result[i]);
	    			$scope.trips.push(trip);
	            }
			}

			if($scope.trips < 1){
				$scope.tripEmpty = true;
			}

			$scope.tripLoader = false; //once loaded, set it false
			$scope.$broadcast('scroll.refreshComplete'); //send complete signal
		});
	}

	$scope.createItinerary = function(){
		ModalCreateItinerary
	      .init($scope)
	      .then(function(modal) {
	      	//once ready show modal
	        modal.show();
	    });
	}
})
.controller('TripDetailCtrl', function($scope, $state, $ionicPlatform, $stateParams){

	$scope.tripID = "";
	$scope.tripName = "";

	$ionicPlatform.ready(function(){
		$scope.tripID = $stateParams.tripID;
		$scope.tripName = $stateParams.tripName;
		$scope.title = $stateParams.tripName;
	});

	$scope.goToTripHotel = function(tripid){
		$state.go('tab.trip-hotel', {"tripID":tripid});
	}

	$scope.goToTripMap = function(){
		$state.go('tab.trip-map');
	}

})
.controller('TripMapCtrl', function($scope, $state, $ionicPlatform, $stateParams, $cordovaGeolocation){

	$ionicPlatform.ready(function(){
		
	});

	var options = {timeout: 1000, enableHighAccuracy: false, maximumAge: 5000};
	$cordovaGeolocation.getCurrentPosition(options).then(function(position){
	    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	    var mapOptions = {
	      center: latLng,
	      zoom: 15,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	 
	    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
	    var marker = new google.maps.Marker({
          position: latLng,
          map: $scope.map,
          title: 'Hello World!'
        });
	 
	}, function(error){
	    console.log("Could not get location " + JSON.stringify(error));
	    var latLng = new google.maps.LatLng(65.9667, -18.5333);
	    var mapOptions = {
	      center: latLng,
	      zoom: 15,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	 
	    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
	    var marker = new google.maps.Marker({
          position: latLng,
          map: $scope.map,
          title: 'Hello World!'
        });
	});

})
.controller('TripHotelCtrl', function($scope, $ionicPlatform, $stateParams, TripService, ModalHotel, $model){

	$scope.tripID = "";
	$scope.triphotels = [];
	$scope.hotelEmpty = false;
	$scope.hotelLoader = true;

	$ionicPlatform.ready(function(){
		$scope.tripID = $stateParams.tripID;
		console.log($scope.tripID);

		$scope.hotelEmpty = true;
		$scope.hotelLoader = false;
	});

	$scope.openModalHotel = function(){
		ModalHotel.init($scope).then(function(modal){
			//once ready show modal
	        modal.show();
		});
	};

	$scope.doReloadTripHotel = function(){
		$scope.triphotels = []; //reset
		TripService.loadTripHotel(1).then(function(result){
			if(result != null){
				for(var i = 0; i < result.length; i++){
	              	var trip = new $model.TripHotel(result[i]);
	    			$scope.triphotels.push(trip);
	            }

	            if($scope.triphotels.length > 0){
	            	$scope.hotelEmpty = false;
	            }else{
	            	$scope.hotelEmpty = true;
	            }
			}

			$scope.$broadcast('scroll.refreshComplete'); //send complete signal
		});
	}

});